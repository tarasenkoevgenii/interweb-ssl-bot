<?php

Route::view('/', 'app');

Route::prefix('/botman')->group(function () {
    Route::post('/', 'BotmanController@index');
});

Route::get('/subscribers/fetch', 'SubscriberController@fetch');
Route::post('/mailing/send', 'MailingController@send');
