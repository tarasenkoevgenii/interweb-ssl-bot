<?php

namespace App\Providers;

use App\Http\Repositories\Interfaces\SubscribersInterface;
use App\Repositories\Subscribers;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SubscribersInterface::class, function ($app) {
            return Subscribers::make();
        });
    }
}
