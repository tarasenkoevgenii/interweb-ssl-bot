<?php

namespace App\Http\Controllers;

use App\Services\BotmanService;

class BotmanController extends Controller
{
    public function index()
    {
        BotmanService::make()->hears();
    }
}
