<?php

namespace App\Http\Controllers;

use App\Http\Requests\SendMailingRequest;
use App\Services\Mailing;
use Illuminate\Http\JsonResponse;

class MailingController extends Controller
{
    public function send(SendMailingRequest $request): JsonResponse
    {
        Mailing::make()->send($request);
        return response()->json(['status' => 'success']);
    }
}
