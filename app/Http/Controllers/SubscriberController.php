<?php

namespace App\Http\Controllers;

use App\Http\Repositories\Interfaces\SubscribersInterface;
use Illuminate\Http\JsonResponse;

class SubscriberController extends Controller
{
    const COUNT_BY_PAGE = 5;

    public function fetch(SubscribersInterface $subscribersRepo): JsonResponse
    {
        $subscribers = $subscribersRepo->paginate(self::COUNT_BY_PAGE);
        return response()->json($subscribers);
    }
}
