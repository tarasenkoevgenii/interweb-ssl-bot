<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed sendToAll
 * @property mixed subscribers
 * @property mixed message
 */
class SendMailingRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'message' => 'required|string|max:2000',
            'subscribers.*' => 'integer',
            'sendToAll' => 'required|boolean',
        ];
    }
}
