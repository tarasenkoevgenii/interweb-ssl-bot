<?php

namespace App\Jobs;

use App\Services\BotmanService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendMailing implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $message;
    protected $subscribers;

    public function __construct(string $message, array $subscribers)
    {
        $this->message = $message;
        $this->subscribers = $subscribers;
    }

    public function handle()
    {
        BotmanService::make()->sendMailing($this->message, $this->subscribers);
    }
}
