<?php

namespace App\Services;

use App\Http\Repositories\Interfaces\SubscribersInterface;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\BotMan;
use BotMan\Drivers\Telegram\TelegramDriver;
use Illuminate\Support\Facades\Log;
use Spatie\SslCertificate\SslCertificate;

class BotmanService
{
    protected $botman;

    public function __construct()
    {
        $this->botman = BotManFactory::create(config('botman'));
    }

    public static function make(): BotmanService
    {
        return new static();
    }

    public function hears()
    {
        $this->botman->hears('ssl-info {domain}', '\App\Services\BotmanService@sslInfo');
        $this->botman->hears('subscribe', '\App\Services\BotmanService@subscribe');
        $this->botman->hears('unsubscribe', '\App\Services\BotmanService@unsubscribe');

        $this->botman->listen();
    }

    public function sslInfo(BotMan $bot, string $domain)
    {
        // В данном случае принял решение использоват try...catch потому, что
        // в случае валидной строки домена при его несуществовании или недоступности
        // createForHostName бросит исключение. [ if((new Domain)->passes('domain', $domain)) ]
        try {
            $certificate = SslCertificate::createForHostName($domain);

            $bot->reply('Issuer: ' . $certificate->getIssuer());
            $bot->reply('Is valid: ' . ($certificate->isValid() ? 'true' : 'false'));
            $bot->reply('Expired in: ' . $certificate->expirationDate()->diffInDays());
        } catch (\Exception $e) {
            $bot->reply('Error! Check domain again');
        }
    }

    public function subscribe(BotMan $bot)
    {
        $user = $bot->getUser();
        $subscribersRepo = app()->make(SubscribersInterface::class);
        $subscriber = $subscribersRepo->find($user->getId());

        if ($subscriber) {
            $bot->reply('You are already subscribed');
        } else {
            $subscribersRepo->create([
                'id' => $user->getId(),
                'first_name' => $user->getFirstName(),
                'last_name' => $user->getLastName(),
            ]);
            $bot->reply('You are subscribed');
        }
    }

    public function unsubscribe(BotMan $bot)
    {
        $user = $bot->getUser();
        $subscribersRepo = app()->make(SubscribersInterface::class);
        $subscribersRepo->delete($user->getId());
        $bot->reply('You have unsubscribed');
    }

    public function sendMailing(string $message, array $subscribers)
    {
        foreach ($subscribers as $subscriber) {
            try {
                $this->botman->say($message, $subscriber, TelegramDriver::class);
            } catch (\Exception $exception) {
                Log::error('Mailing error: ' . $exception->getMessage());
            }
        }

        $this->botman->listen();
    }
}