<?php

namespace App\Services;

use App\Http\Repositories\Interfaces\SubscribersInterface;
use App\Http\Requests\SendMailingRequest;
use App\Jobs\SendMailing;

class Mailing
{
    public static function make(): Mailing
    {
        return new static();
    }

    public function send(SendMailingRequest $request)
    {
        $subscribersRepo = app()->make(SubscribersInterface::class);
        if($request->sendToAll) $subscribers = $subscribersRepo->allIds();
        else $subscribers = $subscribersRepo->getSubscribersIn($request->subscribers);

        SendMailing::dispatch($request->message, $subscribers);
    }
}