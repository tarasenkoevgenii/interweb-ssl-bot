<?php

namespace App\Http\Repositories\Interfaces;


interface SubscribersInterface extends BaseRepositoryInterface
{
    public static function make();

    public function allIds(): array;
}