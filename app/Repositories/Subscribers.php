<?php

namespace App\Repositories;

use App\Http\Repositories\Interfaces\SubscribersInterface;
use App\Models\Subscriber;

class Subscribers extends BaseRepository implements SubscribersInterface
{
    public function __construct(Subscriber $subscriber)
    {
        parent::__construct($subscriber);
    }

    public static function make(): Subscribers
    {
        return new static(new Subscriber);
    }

    public function allIds(): array
    {
        return $this->model->get(['id'])->pluck('id')->toArray();
    }

    public function getSubscribersIn(array $ids): array
    {
        return $this->model->whereIn('id', $ids)->get(['id'])->pluck('id')->toArray();
    }
}