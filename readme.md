## SSL Telegram bot

Для проверки SSL-сертификата введите: `ssl-info {domain}`.

Для подписки\отписки на рассылку `subscribe`/`unsubscribe` соответсвенно.

Для тестирования рассылок: [https://greenstreet.com.ua/](https://greenstreet.com.ua/)

Небольшая видео демонтрация: [https://youtu.be/GW73dWP763s](https://youtu.be/GW73dWP763s) 