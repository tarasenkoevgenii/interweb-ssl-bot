import Vue from 'vue';
import Vuex from 'vuex';
import global from './modules/global';
import mailing from './modules/mailing';

Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        global: global,
        mailing: mailing,
    },
});