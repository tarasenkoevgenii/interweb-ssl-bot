const state = {
    subscribersPaginator: {},
    selectedSubscribers: [],
    sendToAll: false,
    countRecipients: 0,
};

const getters = {
    isSelected: state => id => {
        return state.selectedSubscribers.indexOf(id) !== -1;
    },
    totalSubscribers: state => {
        return state.subscribersPaginator.total;
    },
    subscribers: state => {
        return state.subscribersPaginator.data ? state.subscribersPaginator.data : [];
    },
};

const actions = {};

const mutations = {
    setSubscribersPaginator(state, paginator) {
        state.subscribersPaginator = paginator;
    },
    changeSubscriber(state, id) {
        let index = state.selectedSubscribers.indexOf(id);

        if(index !== -1) Vue.delete(state.selectedSubscribers, id);
        else state.selectedSubscribers.push(id);

        state.countRecipients = state.selectedSubscribers.length;
    },
    deleteSelected(state) {
        state.selectedSubscribers = [];
        state.countRecipients = state.selectedSubscribers.length;
    },
    changeSendToAll(state) {
        state.sendToAll = !state.sendToAll;
        if (state.sendToAll) state.countRecipients = state.subscribersPaginator.total;
        else state.countRecipients = state.selectedSubscribers.length;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};