export const responseProcessing = {
    data: () => ({
        activeQuery: null,
        promise: null,
        errors: [],
    }),
    methods: {
        /**
         * Начало запроса либо ошибка.
         * Испльзуется для того, чтобы предотвратить несколько подряд отправленнх запрсов.
         */
        startOrFailIfIsset() {
            if (!this.activeQuery) {
                this.startQuery();
                return true;
            } else {
                this.promise = this.rejected('ISSET_QUERY');
                return false;
            }
        },
        /**
         * Выполнение запроса.
         */
        executePromise() {
            return this.promise.then((response) => {
                return this.successQuery(response);
            }).catch(error => {
                return this.checkErrors(error);
            });
        },
        /**
         * Успешное завершение запроса
         */
        successQuery(response) {
            this.clearErrors();
            this.endQuery();

            return Promise.resolve(response);
        },
        /**
         * Обработка ошибок
         */
        checkErrors(error) {
            if (error.response) {
                if (error.response.status === 401) this.$toaster.error('Authorization required');
                if (error.response.status === 403) this.$toaster.error('Not enough rights');
                if (error.response.status === 404) this.$toaster.error('Page not found');
                if (error.response.status === 419) this.$toaster.error('The page has expired due to inactivity. Update and try again');
                if (error.response.status === 500) this.$toaster.error('Server error');
                if (error.response.status === 422) this.errors = error.response.data.errors;
                if (error.response.status === 499) {
                    for (let key in error.response.data) {
                        this.$toaster.error(error.response.data[key])
                    }
                }
            }else if (typeof error === 'string') {
                this.$toaster.error(error);
            }

            this.endQuery();
            return Promise.reject(error);
        },
        /**
         * Очистка ошибкок.
         */
        clearErrors() {
            this.errors = [];
        },
        /**
         * Начало зпроса.
         */
        startQuery() {
            this.activeQuery = true;

        },
        /**
         * Завернешие выполненного запроса.
         */
        endQuery() {
            this.activeQuery = false;
            this.promise = null;
        },
        /**
         * Отклонение запроса с ошибкой.
         */
        rejected(type) {
            let massages = {
                'ISSET_QUERY': 'Please wait until the end of the previous request.',
                'UNKNOWN': 'Unknown error',
            };

            return Promise.reject(massages[type] ? massages[type] : massages['UNKNOWN']);
        }
    }
};