import {responseProcessing} from './responseProcessing';

const prefix = '/subscribers';

export const subscribersAPI = {
    mixins: [responseProcessing],
    methods: {
        fetchAPI(page) {
            this.promise = axios.get(`${prefix}/fetch?page=${page}`);

            return this.executePromise();
        },
    }
};