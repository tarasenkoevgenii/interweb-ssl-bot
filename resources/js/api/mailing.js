import {responseProcessing} from './responseProcessing';

const prefix = '/mailing';

export const mailingAPI = {
    mixins: [responseProcessing],
    methods: {
        sendAPI(data) {
            if (this.startOrFailIfIsset())
                this.promise = axios.post(`${prefix}/send`, data);

            return this.executePromise();
        },
    }
};